# Template file for 'cbqn'
pkgname=cbqn
version=0.7.0
revision=1
archs='x86_64* aarch64*'

maintainer='Coral Pink <coral.pink@disr.it>'
short_desc='BQN implementation in C '

# src/builtins/sortTemplate.h - MIT
# src/utils/ryu.c, src/utils/ryu/* - Apache 2.0/Boost 1.0
# * - GNU LGPLv3 only/GNU GPLv3 only/MPL 2.0
license='Apache-2.0, MIT, BSL-1.0, LGPL-3.0-only, GPL-3.0-only, MPL-2.0'
homepage='https://github.com/dzaima/CBQN'

_cbqnBytecodeRev=c5674783c11d7569e5a4d166600ffcde2409331d
_replxxRev=13f7b60f4f79c2f14f352a76d94860bad0fc7ce9
_singeliRev=ce6ef5d06d35777f0016bbfe0c6c1cf6a9c1b48e

distfiles="
	https://github.com/dzaima/CBQN/archive/refs/tags/v${version}.tar.gz>cbqn.tar.gz
	https://github.com/dzaima/cbqnBytecode/archive/${_cbqnBytecodeRev}.zip>cbqnBytecode.zip
	https://github.com/dzaima/replxx/archive/${_replxxRev}.zip>replxx.zip
	https://github.com/mlochbaum/Singeli/archive/${_singeliRev}.zip>singeli.zip
"

checksum='
	2423ff6f5d4b0dce20a084369149a4fa0f9e2d6c358bcd2fe986ba0ec62ce350
	799278ff5699b384bfdba72dd73c0db04e9c7a95df865ea4bbae34488f0a9549
	e4a30523e691d155a13d6c77eda982ffe7b5dbec724ebde1f8701401466483c6
	056441effa098922a337f96dbaffe299164fe613dc134f779f2581f8a1fc38e3
'

hostmakedepends=''
makedepends="pkg-config $(vopt_if 'ffi' 'libffi-devel')"

build_options='ffi replxx'
build_options_default='ffi replxx'
desc_option_ffi='Enable •FFI'
desc_option_replxx='Enable REPLXX'

do_extract() {
	vsrcextract 'cbqn.tar.gz'
	vsrcextract -C './build/bytecodeLocal' 'cbqnBytecode.zip'
	vsrcextract -C './build/replxxLocal' 'replxx.zip'
	vsrcextract -C './build/singeliLocal' 'singeli.zip'
}

do_build() {
	local target="${XBPS_TARGET_MACHINE/-musl/}"

	make o3 shared-o3 \
		ENABLE_PIE=1 \
		notui=1 \
		target_arch="$target" \
		version="${version}" \
		FFI="${build_option_ffi:-0}" \
		REPLXX="${build_option_replxx:-0}" \
		CCFLAGS="$([ "$target" = 'aarch64' ] && echo '-flax-vector-conversions')" \
		LDFLAGS="$LDFLAGS_host"
}

do_install() {
	vbin 'BQN' 'bqn'
	vinstall 'include/bqnffi.h' 644 'usr/include'
	vinstall 'libcbqn.so' 755 'usr/lib' "libcbqn.so.${version}"
	ln -sf "libcbqn.so.${version}" "${DESTDIR}/usr/lib/libcbqn.so"

	for license in "./licenses"/*; do
		vlicense "$license"
	done
}

libcbqn_package() {
	short_desc+=" - library"
	pkg_install() {
		vmove "usr/lib/*.so.*"
	}
}

libcbqn-devel_package() {
	short_desc+=" - development files"
	depends="libcbqn>=${version}_${revision}"
	pkg_install() {
		vmove "usr/lib/*.so"
		vmove 'usr/include'
	}
}
