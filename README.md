# void linux considered harmful - install gentoo

gentoo's overlays are far better design and integrate nicely into the operating system, besides the main benefit of not being a [virtue signaling](https://nigger.poast.org/VoidLinux/status/1320862017582997504#m) distro, which has been an ongoing issue since then for the core team members

## what

a collection of void linux/xbps template files for various packages

## which
| package | version | homepage | description | notes |
|---------|---------|----------|-------------|-------|
| [`berry-lang`](./srcpkgs/berry-lang/template) | `1.1.0` | [berry-lang.github.io](https://berry-lang.github.io/) | ultra-lightweight embedded scripting language optimized for microcontrollers |
| [`cbqn`](./srcpkgs/cbqn/template) | `0.7.0` | [github.com/dzaima/CBQN](https://github.com/dzaima/CBQN) | BQN implementation in C |
| [`crystal`](./srcpkgs/crystal/template) | `1.13.1` | [crystal-lang.org](https://crystal-lang.org/) | Crystal Programming Language |
| [`drasl`](./srcpkgs/drasl/template) | `1.1.1` | [github.com/unmojang/drasl](https://github.com/unmojang/drasl) | Yggdrasil-compatible API server for Minecraft |
| [`FjordLauncher`](./srcpkgs/FjordLauncher/template) | `8.4.1` | [github.com/unmojang/FjordLauncher](https://github.com/unmojang/FjordLauncher) | custom launcher for Minecraft | patched out [microsoft account requirement](./srcpkgs/FjordLauncher/patches/00-allow-no-microsoft-account.diff) (behaviour before [`8.3.2`](https://github.com/unmojang/FjordLauncher/compare/8.3.1...8.3.2))
| [`fuzzylite`](./srcpkgs/fuzzylite/template) | `6.0` | [fuzzylite.com](https://fuzzylite.com/) | fuzzy logic control library in C++ |
| [`koka`](./srcpkgs/koka/template) | `3.1.0` | [koka-lang.github.io](https://koka-lang.github.io/koka/) |  Koka language compiler and interpreter |
| [`nickel`](./srcpkgs/nickel/template) | `1.7.0` | [nickel-lang.org](https://nickel-lang.org/) | generic configuration language |
| [`PollyMC`](./srcpkgs/PollyMC/template) | `8.0` | [github.com/fn2006/PollyMC](https://github.com/fn2006/PollyMC) | custom launcher for Minecraft | seems unmaintained, consider using `FjordLauncher` fork instead
| [`prometheus-cpp-lite`](./srcpkgs/prometheus-cpp-lite/template) | `1.0` | [github.com/biaks/prometheus-cpp-lite](https://github.com/biaks/prometheus-cpp-lite) | C++ header-only prometheus client library |
| [`shards`](./srcpkgs/shards/template) | `0.18.0` | [github.com/crystal-lang/shards](https://github.com/crystal-lang/shards) | dependency manager for the Crystal language |
| [`uiua`](./srcpkgs/uiua/template) | `0.11.1` | [www.uiua.org](https://www.uiua.org/) | array-oriented stack programming language |
| [`vcmi`](./srcpkgs/vcmi/template) | `1.5.5` | [vcmi.eu](https://vcmi.eu/) | open-source engine for Heroes of Might and Magic III |
| [`zerotier-one`](./srcpkgs/zerotier-one/template) | `1.14.0` | [zerotier.com](https://zerotier.com/) | smart ethernet switch for earth |

### personal

| package | version | repository | description |
|---------|---------|----------|-------------|
| [`debounce`](./srcpkgs/debounce/template) | `0.1.2` | [debounce](https://codeberg.org/coralpink/debounce) | utility for debouncing lines from stdin
| [`unshell`](./srcpkgs/unshell/template) | `0.9.0` | [unshell](https://codeberg.org/coralpink/unshell) | utility for splitting input into shell-like tokens

## how

> this could have been done way easier, but alas, thanks void - I am not going to fork the entire packages repository and rebase it every time just so I can manage a few packages with xbps

you may want to read [this](https://github.com/void-linux/void-packages#quick-start) on how xbps templates work

in short:
- clone [`void-packages`](https://github.com/void-linux/void-packages) repository
- clone this repository
- copy `srcpkgs/*` from this repo to `srcpkgs` in `void-packages` repo
- append `common/shlibs` from this repo to `common/shlibs` in `void-packages` repo
- run `./xbps-src binary-bootstrap` in `void-packages` repo
- run `./xbps-src pkg 'package-name'` in `void-packages` repo to build `'package-name'`
- run `xbps-install --repository ./hostdir/binpkgs 'package-name'` in `void-packages` repo to install `'package-name'`

there is a short shell script included [`install.sh`](./install.sh) for one-off package installation which does exactly that
```sh
./install.sh 'package-name'
```

if you feel daring feel free to pipe `curl` output directly into shell (but please take a peek to inspect what it does beforehand):
```sh
curl -fsSL 'https://codeberg.org/coralpink/void/raw/branch/master/install.sh' -o - | sh -s -- 'package-name'
```
